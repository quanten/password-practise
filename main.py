#!/bin/env python3
import getpass
import time

password = getpass.getpass("Password: ")

if input("see password to verify? (y): ") == "y":
    input(password + " (enter to hide)...")
    print('\x1b[1A' + '\x1b[2K')

print("practise now!")
while True:
    start = time.time()
    typed_password = getpass.getpass("Password: ")
    end = time.time()
    if typed_password == password:
        print("correct!")
        print("Time needed: {time} and {chrs} chr/sec".format(time=end-start,chrs=len(typed_password)/(end-start)))
    else:
        print("WRONG")
    print()